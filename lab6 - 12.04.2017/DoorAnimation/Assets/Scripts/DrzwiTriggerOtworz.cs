﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrzwiTriggerOtworz : MonoBehaviour {

	private Animator animator = null;
	public GameObject zawias;

	void Start()
	{
		animator = zawias.GetComponent<Animator> ();
	}

	void OnTriggerEnter(Collider other)
	{
		animator.SetBool ("czyOtwarte", true);
	}
}
