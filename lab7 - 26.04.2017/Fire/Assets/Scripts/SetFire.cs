﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetFire : MonoBehaviour {

	public ParticleSystem ogien;
	public ParticleSystem dym;

	void Update()
	{
		if (ogien.isPlaying) {
			if (Input.GetKeyDown (KeyCode.F1)) {
				ogien.Stop ();
			} 
		} 
		else 
		{
			if (Input.GetKeyDown (KeyCode.F1)) {
				ogien.Play ();
			} 
		}

		if (dym.isPlaying) {
			if (Input.GetKeyDown (KeyCode.F2)) {
				dym.Stop ();
			} 
		} 
		else 
		{
			if (Input.GetKeyDown (KeyCode.F2)) {
				dym.Play ();
			} 
		}

		if (dym.isPlaying && ogien.isPlaying) {
			if (Input.GetKeyDown (KeyCode.F3)) {
				dym.Stop ();
				ogien.Stop ();
			} 
		} 
		else 
		{
			if (Input.GetKeyDown (KeyCode.F3)) {
				dym.Play ();
				ogien.Play ();
			} 
		}
	}
}
