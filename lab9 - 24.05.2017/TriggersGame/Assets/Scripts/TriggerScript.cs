﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerScript : MonoBehaviour {

	public GameObject LightG;
	void OnTriggerEnter(Collider other)
	{
		if (other.GetComponent<Collider>().CompareTag("Player"))
		{
			LightG.GetComponent<Light>().color = Color.yellow;
		}
	}
	void OnTriggerStay(Collider other)
	{
		if (other.GetComponent<Collider>().CompareTag("Player"))
		{
			LightG.GetComponent<Light>().color = Color.red;
		}
	}
	void OnTriggerExit(Collider other)
	{
		if (other.GetComponent<Collider>().CompareTag("Player"))
		{
			LightG.GetComponent<Light>().color = Color.yellow;
		}
	}
}
