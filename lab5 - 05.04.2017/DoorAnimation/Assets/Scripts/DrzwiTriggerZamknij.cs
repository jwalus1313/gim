﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrzwiTriggerZamknij : MonoBehaviour {

	private Animator animator = null;
	public GameObject zawias;

	void Start()
	{
		animator = zawias.GetComponent<Animator> ();
	}
	
	void OnTriggerExit(Collider other)
	{
		animator.SetBool ("czyOtwarte", false);
	}
}
